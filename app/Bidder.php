<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bidder extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bidders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'surname', 'personal_number'];

    public function auction()
    {
        return $this->hasOne('App\Auction');
    }

}
