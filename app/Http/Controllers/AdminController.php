<?php

namespace App\Http\Controllers;

use App\Item;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $myItems = $request->user()->itemsForSale;
        return view('admin.index', ['myItems' => $myItems]);
    }

    public function newItem(Request $request)
    {
        $item = new Item();

        if ($request->isMethod('POST')){

            $this->validate($request, [
                'address' => 'required',
                'area' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
            ]);

            $input = $request->all();
            $item->fill($input);
            $item->seller_id = $request->user()->id;
            $item->save();
            $request->session()->flash('status', 'Task was successful!');

            return redirect()->route('admin');
        }

        return view('admin.new_item');
    }

    public function newAdmin()
    {
        $roles = Role::all();
        return view('admin.register', ['roles' => $roles]);
    }

    public function confirm($confirmationCode)
    {
        $user = User::where('confirmation_code', $confirmationCode)->first();

        if(!$user){
            abort(404);
        }

        $user->confirmed = 1;
        $user->save();

        if (\Auth::check()) {
            \Auth::logout();
        }

        return redirect()->route('login.get');
    }

    public function edit(Requests\EditItemRequest $request, $id)
    {
        $item = Item::find($id);
        return view('admin.edit-item', ['item' => $item]);
    }
}
