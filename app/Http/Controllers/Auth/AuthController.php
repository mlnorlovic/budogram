<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if(\Auth::user()){
            if (\Auth::user()->roles->contains(Role::where('name', 'ROLE_ADMIN')->first()->id)) {
                return '/admin';
            }
        }else{
            return '/';
        }
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout', 'getRegister', 'postRegister']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'phone' => 'required|max:255|regex:/^[0-9]+$/|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ];

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(30);
        $user = User::create([
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code
        ]);

        $roles = isset($data['roles']) ? $data['roles'] : Role::where('name', 'ROLE_CLIENT')->first()->id;
        $user->roles()->attach($roles);

        \Mail::send('emails.welcome', ['confirmation_code' => $confirmation_code], function($message) {
            $message->from('4058464@gmail.com', 'Laravel');
            $message->to('4058464@gmail.com')
                ->subject('Verify your email address');
        });
        return $user;
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        return redirect($this->redirectPath());
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $this->getCredentials($request);

        if (\Auth::validate($credentials)) {
            $user = \Auth::getLastAttempted();
            if ($user->confirmed) {
                \Auth::login($user, $request->has('remember'));
                return redirect()->intended($this->redirectPath());
            } else {
                return redirect($this->loginPath())
                ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'confirmed' => 'You have to confirm your email in order to login'
                    ]);
            }
        }

        return redirect($this->loginPath())
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }
}
