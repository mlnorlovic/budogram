<?php
namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankIdController extends Controller
{
    public function soapCall()
    {
        $localCert = "/Users/nicknamed/projects/budogram/cert.pem";
        $wsdl = 'https://appapi.test.bankid.com/rp/v4?wsdl';

        $client = new \SoapClient($wsdl, [
            "trace" => 1,
            "exceptions" => 1,
            "local_cert" => $localCert
        ]);

        return $client;
    }

    public function soap()
    {
        $client = $this->soapCall();
        $parameters = [
            'personalNumber' => '198108080154',
            'requirementAlternatives' => [
                'requirement' => [
                    'condition' => [
                        'key' => 'CertificatePolicies',
                        'value' => '1.2.3.4.25'
                    ]
                ]
            ]
        ];
        $soapResponse = $client->Authenticate($parameters);
        new Request(['bankid:///']);

        return new JsonResponse(['orderRef' => $soapResponse->orderRef]);
    }

    public function collect(Request $request)
    {
        $client = $this->soapCall();

        $soapResponse = $client->Collect($request->request->get('orderRef'));

        return new JsonResponse(['data' => $soapResponse]);
    }
}