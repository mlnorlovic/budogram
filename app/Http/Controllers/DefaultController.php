<?php

namespace App\Http\Controllers;

use App\Auction;
use App\Bidder;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DefaultController extends Controller
{
    public function home()
    {
        return view('welcome');
    }

    public function itemsList(Request $request)
    {
        $today = new \DateTime();
        $items = Item::where('start_date', '<=', $today)->where('end_date', '>=', $today)->paginate(4);


        if ($request->user()) {
            $myItemsObject = $request->user()->items;

            $myItems = [];

            foreach($myItemsObject as $item)
            {
                $myItems[] .= $item->id;
            }
        } else{
            $myItems = [];
        }
        if($request->isMethod('POST')){
            $search = $request->input('search');
            $items = Item::where('start_date', '<=', $today)->where('end_date', '>=', $today)->where('id', $search)->paginate(4);

            return view('default.items_list', ['items' => $items, 'myItems' => $myItems]);
        }

        return view('default.items_list', ['items' => $items, 'myItems' => $myItems]);
    }

    public function show(Requests\ShowItemRequest $request, $id)
    {
        $item = Item::find($id);

        $auctions = Auction::where('item_id', $item->id)->get();
        $max = Auction::where('item_id', $item->id)->max('price');

        $start = new \DateTime();
        $end = new \DateTime($item->end_date);

        $diff =$start->diff($end)->days;

        return view('default.show_item', [
             'item' => $item,
             'auctions' => $auctions,
             'max' => $max,
             'diff' => $diff
            ]
        );
    }

    public function myItems(Request $request)
    {
        $items = $request->user()->items;
        return view('default.my_items', ['items' => $items]);
    }

    public  function newAuction(Requests\CreateAuctionRequest $request, $id)
    {
        $item = Item::find($id);

        $bidder = new Bidder();
        $bidder->name = $request->input('name');
        $bidder->surname = $request->input('surname');
        $bidder->personal_number = $request->input('personalNumber');
        $bidder->save();

        $auction = new Auction();
        $auction->price = $request->input('price');
        $auction->item_id = $item->id;
        $auction->user_id = $request->user()->id;
        $auction->bidder_id = $bidder->id;
        $auction->save();

        if (!$item->users->contains($request->user()->id)) {
            $item->users()->attach($request->user()->id);
        }

        return redirect()->route('show-item', ['id' => $item->id]);
    }
}