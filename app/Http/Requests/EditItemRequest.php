<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $itemId = $this->route('id');
        $currentUser = $this->user();

        if (!$currentUser->itemsForSale->contains($itemId))
        {
            abort(403);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
