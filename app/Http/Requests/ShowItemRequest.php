<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Item;

class ShowItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $item = Item::where('id', $this->route('id'))->first();

        if (!$item)
        {
            abort(404);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
