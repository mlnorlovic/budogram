<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home',
    'uses' => 'DefaultController@home'
]);

Route::get('auth/login', ['as' => 'login.get', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'login.post', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::get('auth/register', [
    'as' => 'reg.get',
    'uses' => 'Auth\AuthController@getRegister'
]);

Route::post('auth/register', [
    'as' => 'reg.post',
    'uses' => 'Auth\AuthController@postRegister'
]);

Route::get('/admin', [
    'as' => 'admin',
    'uses' => 'AdminController@index',
    'middleware' => 'auth'
]);

Route::any('/admin/new-item', [
    'as' => 'new',
    'uses' => 'AdminController@newItem',
    'middleware' => 'auth'
]);

Route::any('/items-list', [
    'as' => 'list',
    'uses' => 'DefaultController@itemsList'
]);

Route::get('/show/{id}', [
    'as' => 'show-item',
    'uses' => 'DefaultController@show'
]);

Route::get('/my-items', [
    'as' => 'my_items',
    'uses' => 'DefaultController@myItems'
]);

Route::post('/soap', [
    'as' => 'soap',
    'uses' => 'BankIdController@soap'
]);

Route::post('/collect', [
    'as' => 'collect',
    'uses' => 'BankIdController@collect'
]);

Route::post('/new-auction/{id}', [
    'as' => 'new-auction',
    'uses' => 'DefaultController@newAuction'
]);

Route::get('/admin/register', [
    'as' => 'admin-register',
    'uses' => 'AdminController@newAdmin',
    'middleware' => 'auth'
]);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirm-path',
    'uses' => 'AdminController@confirm'
]);

Route::get('/admin/edit/{id}', [
    'as' => 'edit-item',
    'uses' => 'AdminController@edit'
]);