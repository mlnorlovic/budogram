<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address', 'area', 'start_date', 'end_date'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'items_users', 'item_id', 'user_id');
    }

    public function auction()
    {
        return $this->hasOne('App\Auction');
    }

    public function seller()
    {
        return $this->belongsTo('App\User', 'seller_id', 'id');
    }
}
