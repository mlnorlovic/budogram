<?php

namespace App\Validators;

use Illuminate\Http\Request;
use App\Auction;

class PriceValidator
{
    protected $request;
    protected $auction;

    public function __construct(Request $request, Auction $auction)
    {
        $this->request = $request;
        $this->auction = $auction;
    }

    public function validateMinPrice($attribute, $value, $parameters, $validator)
    {
        $currentMax = $this->auction->where('item_id', $this->request->route('id'))->max('price');
        return $value > $currentMax;
    }
}