$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

    var authButton = $('.js-auth');

    if($('.js-personalNumber').val() !== ''){
        authButton.hide();
        $('.js-bid').prop('disabled', false);
    }

    authButton.click(function(e){
        e.preventDefault();

        $.ajax({
            url: '/budogram/public/soap',
            type: 'POST'
        }).done(function(response) {

            var interval = setInterval(function(){
                $.ajax({
                    url: '/budogram/public/collect',
                    type: 'POST',
                    data: {
                        orderRef: response.orderRef
                    }
                }).done(function(response) {

                    if (response.data.progressStatus === 'COMPLETE') {
                        $('.js-personalNumber').val(response.data.userInfo.personalNumber);
                        $('.js-name').val(response.data.userInfo.givenName);
                        $('.js-surname').val(response.data.userInfo.surname);
                        $('.js-bid').prop('disabled', false);
                        authButton.hide();
                        clearInterval(interval);
                        $('#myModal').modal('hide');
                    }
                });
            }, 3000);

        });
    });

});