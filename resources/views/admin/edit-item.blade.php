@extends('layout.admin_layout')
@section('content')
    @parent
    <div class="container">

        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                {!! Form::model($item) !!}
                <div class="form-group">
                    {!!  Form::label('address', 'Address') !!}
                    {!!  Form::text('address', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!!  Form::label('area', 'Area') !!}
                    {!!  Form::text('area', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!!  Form::label('start_date', 'Auction start date') !!}
                    {!!  Form::date('start_date', $item->start_date,  ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!!  Form::label('end_date', 'Auction end date') !!}
                    {!!  Form::date('end_date', $item->end_date, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!!  Form::submit('submit',['class' => 'btn btn-large btn-info']) !!}
                    {!! Form::close() !!}
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection


