@extends('layout.admin_layout')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <h4>Register form</h4>
                <form method="POST" action="{{ route('reg.post') }}">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        Phone number
                        <input type="text" name="phone" value="{{ old('phone') }}" class="form-control">
                    </div>

                    <div class="form-group">
                        Email
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                    </div>

                    <div class="form-group">
                        Password
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        Confirm Password
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        Select roles
                        <select multiple class="form-control" name="roles[]">
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-large btn-info">Register</button>
                    </div>
                </form>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection