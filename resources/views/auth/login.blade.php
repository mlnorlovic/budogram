@extends('layout.default')
@section('content')
    @parent
<div class="container">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="row">
            <h4>Login form</h4>
            <form method="POST" action="{{ route('login.post') }}" novalidate="novalidate">
                {!! csrf_field() !!}
                <div class="form-group">
                    Email
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                </div>

                <div class="form-group">
                    Password
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-large btn-info">Login</button>
                </div>
            </form>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection