@extends('layout.default')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                {!! Form::open(array('route' => 'list')) !!}
                {!! Form::text('search', null, ['class'=>'form-control']) !!} <br>
                <div class="text-center">
                    {!! Form::submit('Search',  ['class'=>'btn btn-info btn-block']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="row">
                <h4>List of all active items</h4>
                <div class="table-responsive">
                    <table class="table table-condesed table-striped">
                        <tr class="info">
                            <td>Id</td>
                            <td>Address</td>
                            <td>Area</td>
                            <td>Auction starts</td>
                            <td>Auction ends</td>
                        </tr>
                        @foreach ($items as $item)
                            <tr>
                                <td><a href="{{ route('show-item', $item->id) }}">{{ $item->id }}</a></td>
                                <td><a href="{{ route('show-item', $item->id) }}">{{ $item->address }}</a>
                                    @if (in_array($item->id, $myItems))
                                        <span class="label label-primary" style="background-color: yellowgreen; margin-left: 5px;">My item</span>
                                    @endif
                                </td>
                                <td>{{ $item->area }} m<sup>2</sup></td>
                                <td>{{ date('F d, Y', strtotime($item->start_date)) }}</td>
                                <td>{{ date('F d, Y', strtotime($item->end_date)) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="text-center">
                    {!! str_replace('/?', '?', $items->render()) !!}
                </div>
            </div>
        </div>
    </div>
@endsection