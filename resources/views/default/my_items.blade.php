@extends('layout.default')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                @if(count($items) !== 0)
                <h4>My items</h4>
                <div class="table-responsive">
                    <table class="table table-condesed table-striped">
                        <tr class="info">
                            <td>Id</td>
                            <td>Address</td>
                            <td>Area</td>
                        </tr>
                        @foreach ($items as $item)
                            <tr>
                                <td><a href="{{ route('show-item', $item->id) }}">{{ $item->id }}</a></td>
                                <td><a href="{{ route('show-item', $item->id) }}">{{ $item->address }}</a></td>
                                <td>{{ $item->area }} m<sup>2</sup></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                @else
                    <h3>You don't have any items yet</h3>
                @endif
            </div>
        </div>
    </div>
@endsection