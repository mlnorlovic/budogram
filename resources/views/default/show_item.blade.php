@extends('layout.default')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <h4>Current item</h4>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr class="info">
                            <td>Id</td>
                            <td>Address</td>
                            <td>Area</td>
                        </tr>
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->address }}</td>
                            <td>{{ $item->area }} m<sup>2</sup></td>
                        </tr>
                    </table>
                </div>
                @if($max !== null)
                    <p class="bg-warning" style="padding: 15px;">Highest offer for this item: <strong>{{ number_format($max,2,",",".") }}</strong></p>
                @else
                    <p class="bg-warning" style="padding: 15px;">This item has no offers yet</p>
                @endif
            </div>
            @if (!Auth::guest())
            <div class="row">
                <h4>The auction for this item expires in <strong style="color: yellowgreen; font-size: 25px;">{{ $diff }}</strong> days</h4>
                <h4>Make an offer</h4>
                {!! Form::open(array('route' => ['new-auction', $item->id])) !!}
                {!! Form::text('price', null, ['class'=>'form-control']) !!} <br>
                {!! Form::hidden('name', null, ['class'=>'form-control js-name']) !!}
                {!! Form::hidden('surname', null, ['class'=>'form-control js-surname']) !!}
                {!! Form::hidden('personalNumber', null, ['class'=>'form-control js-personalNumber']) !!}
                {!! Form::submit('Place a bid',  ['class'=>'btn btn-large btn-info js-bid', 'disabled']) !!}
                <button class="js-auth btn btn-large btn-info" data-toggle="modal" data-target="#myModal">Authenticate with Bank Id</button>
                {!! Form::close() !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            @if($max !== null)
            <div class="row">
                <h4>Auctions for this item</h4>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr class="info">
                            <td>Id</td>
                            <td>Price</td>
                            <td>Bidder</td>
                        </tr>
                        @foreach($auctions as $auction)
                            <tr>
                                <td>{{ $auction->id }}</td>
                                <td>{{ number_format($auction->price,2,",",".") }}</td>
                                <td>{{ $auction->bidder->name }} {{ $auction->bidder->surname }}, {{ $auction->bidder->personal_number }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif
            @else
                Yo have to be logged in to make a bid for this item
            @endif
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="position: absolute; top: 200px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <h5>Please open BankId application on your mobile device and enter verification code</h5>
                        <img src="http://localhost/budogram/resources/assets/images/loader2.gif" class="loader" width="150px" height="150px">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection