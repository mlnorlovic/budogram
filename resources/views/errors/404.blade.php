@extends('layout.default')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                <h3>404 - Not found</h3>
            </div>
        </div>
    </div>
@endsection