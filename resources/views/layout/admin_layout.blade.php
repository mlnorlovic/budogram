<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>App Name - @yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

@section('content')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('home') }}">HOME</a></li>
                    <li><a href="{{ route('admin') }}">DASHBOARD</a></li>
                    <li><a href="{{ route('new') }}">NEW ITEM</a></li>
                    <li><a href="{{ route('admin-register') }}">NEW USER</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li> <a href="{{ route('login.get') }}">Login</a></li>
                        <li> <a href="{{ route('reg.get') }}">Register</a></li>
                    @else
                        <li> <a href="{{ route('logout') }}">Logout <span class="glyphicon glyphicon-user"></span> {{Auth::user()->email}}</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@show

</body>
</html>