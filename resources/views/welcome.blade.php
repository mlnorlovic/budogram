@extends('layout.default')
@section('title', 'Page Title')
@section('content')
    @parent
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row text-center">
                <h4>This is the bidding application</h4>
                <h4>where you can bid for the appartments.</h4>
                <h4>Good luck!</h4>
            </div>
        </div>
    </div>
@endsection